package pl.edu.pwr.gg;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class GGClient {

	private static final int PORT = 5000;
	private static final String HOST = "127.0.0.1";
	private JTextArea receivedMessages;
	private JTextField message;
	private BufferedReader reader;
	private PrintWriter writer;
	private Socket socket;
	private JFrame frame;
	
	public static void main(String[] args) {
		GGClient ggClient = new GGClient();
		ggClient.run();
	}

	private void run() {
		frame = createFrame();
		try {
			configureCommunication();
			startReceiverThread();
			showFrame();
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(frame, 
					"Nie uda\u0142o si\u0119 po\u0142\u0105czy\u0107 z serwerem", 
					"B\u0142\u0105d po\u0142\u0105czenia", 
					JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		
	}
	
	private void showFrame() {
		frame.setVisible(true);
		message.requestFocus();		
	}

	private void startReceiverThread() {
		Thread receiver = new Thread(new MessageReceiver());
		receiver.start();		
	}

	private JFrame createFrame() {
		JFrame frame= new JFrame("GG");
		JPanel mainPanel = new JPanel();
		
		receivedMessages = new JTextArea(15,25);
		receivedMessages.setLineWrap(true);
		receivedMessages.setWrapStyleWord(true);
		receivedMessages.setEditable(false);
		
		JScrollPane scroller = new JScrollPane(receivedMessages);
		scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		message = new JTextField(20);
		message.addActionListener(new SendButtonActionListener());
		
		JButton sendButton = new JButton("Wy\u015blij");
		sendButton.addActionListener(new SendButtonActionListener());
		
		mainPanel.add(scroller);
		mainPanel.add(message);
		mainPanel.add(sendButton);
		frame.addWindowListener(new CloseSocketOnWindowClose());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
		frame.setSize(400, 400);
		return frame;
	}

	private void configureCommunication() throws IOException {
		socket = new Socket(HOST, PORT);
		InputStreamReader isr = new InputStreamReader(socket.getInputStream());
		reader = new BufferedReader(isr);
		
		writer = new PrintWriter(socket.getOutputStream());
		
		System.out.println("Po\u0142\u0105czono z serwerem.");
	}
	
	public class CloseSocketOnWindowClose extends WindowAdapter {
		@Override
		public void windowClosed(WindowEvent e) {
			try {
				reader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			writer.close();
			try {
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public class SendButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			writer.println(message.getText());
			writer.flush();
			message.setText("");
			message.requestFocus();
		}
	}
	
	public class MessageReceiver implements Runnable {

		@Override
		public void run() {
			String message;
				try {
					while ((message = reader.readLine()) != null) {
						System.out.println("Odczytano: " + message);
						receivedMessages.append(message);
						receivedMessages.append("\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame, 
							"Utracono po\u0142\u0105czenie z serwerem", 
							"B\u0142\u0105d po\u0142\u0105czenia", 
							JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
		}
		
	}
	
}
