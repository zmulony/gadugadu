package pl.edu.pwr.gg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

public class GGServer {

	private Map<Socket, PrintWriter> clients = new HashMap<Socket, PrintWriter>();

	public static void main(String[] args) {
		new GGServer().start();
	}
	
	private void start() {
		ServerSocket serverSocket = getServerSocket();
		while (serverSocket != null && !serverSocket.isClosed()) {
			Socket clientSocket = acceptConnection(serverSocket);
			createClientManagerThread(clientSocket);
			System.out.println("Skonfigurowano po\u0142\u0105czenie z klientem");
		}
	}

	private ServerSocket getServerSocket() {
		try {
			return new ServerSocket(5000);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}

	private void createClientManagerThread(Socket clientSocket) {
		Thread clientManagement = new Thread(new ClientManager(clientSocket));
		clientManagement.start();		
	}

	private Socket acceptConnection(ServerSocket serverSocket) {
		Socket clientSocket = null;
		try {
			clientSocket = serverSocket.accept();
			PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
			clients.put(clientSocket, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return clientSocket;
	}

	public class ClientManager implements Runnable {
		BufferedReader reader;
		Socket socket;
		
		public ClientManager(Socket clientSocket) {
			try {
				socket = clientSocket;
				InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
				reader = new BufferedReader(isReader);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			String message;
			while ((message = getNextLineFromSocket()) != null) {
				System.out.println("Odczytano: " + message);
				broadcast(message);
			}
		}

		public void broadcast(String message) {
			for (PrintWriter clientWriter : clients.values()) {
				clientWriter.println(message);
				clientWriter.flush();
			}
		}
	
		public String getNextLineFromSocket() {
			String message = null;
			try {
				message = reader.readLine();
			} catch (SocketException e) {
				System.out.println("Utracono po\u0142\u0105czenie z klientem");
				clients.remove(socket);
				System.out.println("Liczba klient\u00f3w: " + clients.size());
				// socket jest już zamknięty
			} catch (IOException e) {
				e.printStackTrace();
			}
			return message;
		}
	}
	
}
